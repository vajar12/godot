// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: interactions.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Interaction {
  public static partial class InteractionService
  {
    static readonly string __ServiceName = "interaction.InteractionService";

    static readonly grpc::Marshaller<global::Interaction.MonConsumeItemRequest> __Marshaller_interaction_MonConsumeItemRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Interaction.MonConsumeItemRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Common.ResponseStatus> __Marshaller_common_ResponseStatus = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Common.ResponseStatus.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Interaction.UserConsumeItemRequest> __Marshaller_interaction_UserConsumeItemRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Interaction.UserConsumeItemRequest.Parser.ParseFrom);

    static readonly grpc::Method<global::Interaction.MonConsumeItemRequest, global::Common.ResponseStatus> __Method_MonsterConsumeItem = new grpc::Method<global::Interaction.MonConsumeItemRequest, global::Common.ResponseStatus>(
        grpc::MethodType.Unary,
        __ServiceName,
        "MonsterConsumeItem",
        __Marshaller_interaction_MonConsumeItemRequest,
        __Marshaller_common_ResponseStatus);

    static readonly grpc::Method<global::Interaction.UserConsumeItemRequest, global::Common.ResponseStatus> __Method_UserConsumeItem = new grpc::Method<global::Interaction.UserConsumeItemRequest, global::Common.ResponseStatus>(
        grpc::MethodType.Unary,
        __ServiceName,
        "UserConsumeItem",
        __Marshaller_interaction_UserConsumeItemRequest,
        __Marshaller_common_ResponseStatus);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Interaction.InteractionsReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of InteractionService</summary>
    [grpc::BindServiceMethod(typeof(InteractionService), "BindService")]
    public abstract partial class InteractionServiceBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Common.ResponseStatus> MonsterConsumeItem(global::Interaction.MonConsumeItemRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Common.ResponseStatus> UserConsumeItem(global::Interaction.UserConsumeItemRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for InteractionService</summary>
    public partial class InteractionServiceClient : grpc::ClientBase<InteractionServiceClient>
    {
      /// <summary>Creates a new client for InteractionService</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public InteractionServiceClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for InteractionService that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public InteractionServiceClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected InteractionServiceClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected InteractionServiceClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Common.ResponseStatus MonsterConsumeItem(global::Interaction.MonConsumeItemRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return MonsterConsumeItem(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Common.ResponseStatus MonsterConsumeItem(global::Interaction.MonConsumeItemRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_MonsterConsumeItem, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> MonsterConsumeItemAsync(global::Interaction.MonConsumeItemRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return MonsterConsumeItemAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> MonsterConsumeItemAsync(global::Interaction.MonConsumeItemRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_MonsterConsumeItem, null, options, request);
      }
      public virtual global::Common.ResponseStatus UserConsumeItem(global::Interaction.UserConsumeItemRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return UserConsumeItem(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Common.ResponseStatus UserConsumeItem(global::Interaction.UserConsumeItemRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_UserConsumeItem, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> UserConsumeItemAsync(global::Interaction.UserConsumeItemRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return UserConsumeItemAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Common.ResponseStatus> UserConsumeItemAsync(global::Interaction.UserConsumeItemRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_UserConsumeItem, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override InteractionServiceClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new InteractionServiceClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(InteractionServiceBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_MonsterConsumeItem, serviceImpl.MonsterConsumeItem)
          .AddMethod(__Method_UserConsumeItem, serviceImpl.UserConsumeItem).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, InteractionServiceBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_MonsterConsumeItem, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Interaction.MonConsumeItemRequest, global::Common.ResponseStatus>(serviceImpl.MonsterConsumeItem));
      serviceBinder.AddMethod(__Method_UserConsumeItem, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Interaction.UserConsumeItemRequest, global::Common.ResponseStatus>(serviceImpl.UserConsumeItem));
    }

  }
}
#endregion
