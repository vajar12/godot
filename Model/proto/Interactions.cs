// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: interactions.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Interaction {

  /// <summary>Holder for reflection information generated from interactions.proto</summary>
  public static partial class InteractionsReflection {

    #region Descriptor
    /// <summary>File descriptor for interactions.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static InteractionsReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChJpbnRlcmFjdGlvbnMucHJvdG8SC2ludGVyYWN0aW9uGhRnb2dvcHJvdG8v",
            "Z29nby5wcm90bxoMY29tbW9uLnByb3RvIlUKFU1vbkNvbnN1bWVJdGVtUmVx",
            "dWVzdBIeCgZpdGVtSWQYASABKAsyDi5jb21tb24uSXRlbUlkEhwKBW1vbklk",
            "GAIgASgLMg0uY29tbW9uLk1vbklkIlgKFlVzZXJDb25zdW1lSXRlbVJlcXVl",
            "c3QSHgoGaXRlbUlkGAEgASgLMg4uY29tbW9uLkl0ZW1JZBIeCgZ1c2VySWQY",
            "AiABKAsyDi5jb21tb24uVXNlcklkIoEBChZJbnRlcmFjdGlvblNlcnZpY2VD",
            "YWxsEiIKCG1ldGFEYXRhGAEgASgLMhAuY29tbW9uLk1ldGFEYXRhEjkKC2Nv",
            "bnN1bWVJdGVtGAIgASgLMiIuaW50ZXJhY3Rpb24uTW9uQ29uc3VtZUl0ZW1S",
            "ZXF1ZXN0SABCCAoGbWV0aG9kIhEKD0ZlZWRNb25SZXNwb25zZSJyChpJbnRl",
            "cmFjdGlvblNlcnZpY2VSZXNwb25zZRIiCghtZXRhRGF0YRgBIAEoCzIQLmNv",
            "bW1vbi5NZXRhRGF0YRIoCgZzdGF0dXMYAiABKAsyFi5jb21tb24uUmVzcG9u",
            "c2VTdGF0dXNIAEIGCgRkYXRhMroBChJJbnRlcmFjdGlvblNlcnZpY2USUgoS",
            "TW9uc3RlckNvbnN1bWVJdGVtEiIuaW50ZXJhY3Rpb24uTW9uQ29uc3VtZUl0",
            "ZW1SZXF1ZXN0GhYuY29tbW9uLlJlc3BvbnNlU3RhdHVzIgASUAoPVXNlckNv",
            "bnN1bWVJdGVtEiMuaW50ZXJhY3Rpb24uVXNlckNvbnN1bWVJdGVtUmVxdWVz",
            "dBoWLmNvbW1vbi5SZXNwb25zZVN0YXR1cyIAQj1aO2dpdGxhYi5jb20vdmFq",
            "YXJfbWluaWdhbWUvbWluaWdhbWVfYmFja2VuZC9kYXRhL2ludGVyYWN0aW9u",
            "YgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Gogoproto.GogoReflection.Descriptor, global::Common.CommonReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Interaction.MonConsumeItemRequest), global::Interaction.MonConsumeItemRequest.Parser, new[]{ "ItemId", "MonId" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Interaction.UserConsumeItemRequest), global::Interaction.UserConsumeItemRequest.Parser, new[]{ "ItemId", "UserId" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Interaction.InteractionServiceCall), global::Interaction.InteractionServiceCall.Parser, new[]{ "MetaData", "ConsumeItem" }, new[]{ "Method" }, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Interaction.FeedMonResponse), global::Interaction.FeedMonResponse.Parser, null, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Interaction.InteractionServiceResponse), global::Interaction.InteractionServiceResponse.Parser, new[]{ "MetaData", "Status" }, new[]{ "Data" }, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class MonConsumeItemRequest : pb::IMessage<MonConsumeItemRequest> {
    private static readonly pb::MessageParser<MonConsumeItemRequest> _parser = new pb::MessageParser<MonConsumeItemRequest>(() => new MonConsumeItemRequest());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<MonConsumeItemRequest> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Interaction.InteractionsReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public MonConsumeItemRequest() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public MonConsumeItemRequest(MonConsumeItemRequest other) : this() {
      itemId_ = other.itemId_ != null ? other.itemId_.Clone() : null;
      monId_ = other.monId_ != null ? other.monId_.Clone() : null;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public MonConsumeItemRequest Clone() {
      return new MonConsumeItemRequest(this);
    }

    /// <summary>Field number for the "itemId" field.</summary>
    public const int ItemIdFieldNumber = 1;
    private global::Common.ItemId itemId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.ItemId ItemId {
      get { return itemId_; }
      set {
        itemId_ = value;
      }
    }

    /// <summary>Field number for the "monId" field.</summary>
    public const int MonIdFieldNumber = 2;
    private global::Common.MonId monId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.MonId MonId {
      get { return monId_; }
      set {
        monId_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as MonConsumeItemRequest);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(MonConsumeItemRequest other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(ItemId, other.ItemId)) return false;
      if (!object.Equals(MonId, other.MonId)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (itemId_ != null) hash ^= ItemId.GetHashCode();
      if (monId_ != null) hash ^= MonId.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (itemId_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(ItemId);
      }
      if (monId_ != null) {
        output.WriteRawTag(18);
        output.WriteMessage(MonId);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (itemId_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(ItemId);
      }
      if (monId_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(MonId);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(MonConsumeItemRequest other) {
      if (other == null) {
        return;
      }
      if (other.itemId_ != null) {
        if (itemId_ == null) {
          ItemId = new global::Common.ItemId();
        }
        ItemId.MergeFrom(other.ItemId);
      }
      if (other.monId_ != null) {
        if (monId_ == null) {
          MonId = new global::Common.MonId();
        }
        MonId.MergeFrom(other.MonId);
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            if (itemId_ == null) {
              ItemId = new global::Common.ItemId();
            }
            input.ReadMessage(ItemId);
            break;
          }
          case 18: {
            if (monId_ == null) {
              MonId = new global::Common.MonId();
            }
            input.ReadMessage(MonId);
            break;
          }
        }
      }
    }

  }

  public sealed partial class UserConsumeItemRequest : pb::IMessage<UserConsumeItemRequest> {
    private static readonly pb::MessageParser<UserConsumeItemRequest> _parser = new pb::MessageParser<UserConsumeItemRequest>(() => new UserConsumeItemRequest());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<UserConsumeItemRequest> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Interaction.InteractionsReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public UserConsumeItemRequest() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public UserConsumeItemRequest(UserConsumeItemRequest other) : this() {
      itemId_ = other.itemId_ != null ? other.itemId_.Clone() : null;
      userId_ = other.userId_ != null ? other.userId_.Clone() : null;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public UserConsumeItemRequest Clone() {
      return new UserConsumeItemRequest(this);
    }

    /// <summary>Field number for the "itemId" field.</summary>
    public const int ItemIdFieldNumber = 1;
    private global::Common.ItemId itemId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.ItemId ItemId {
      get { return itemId_; }
      set {
        itemId_ = value;
      }
    }

    /// <summary>Field number for the "userId" field.</summary>
    public const int UserIdFieldNumber = 2;
    private global::Common.UserId userId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.UserId UserId {
      get { return userId_; }
      set {
        userId_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as UserConsumeItemRequest);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(UserConsumeItemRequest other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(ItemId, other.ItemId)) return false;
      if (!object.Equals(UserId, other.UserId)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (itemId_ != null) hash ^= ItemId.GetHashCode();
      if (userId_ != null) hash ^= UserId.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (itemId_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(ItemId);
      }
      if (userId_ != null) {
        output.WriteRawTag(18);
        output.WriteMessage(UserId);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (itemId_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(ItemId);
      }
      if (userId_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(UserId);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(UserConsumeItemRequest other) {
      if (other == null) {
        return;
      }
      if (other.itemId_ != null) {
        if (itemId_ == null) {
          ItemId = new global::Common.ItemId();
        }
        ItemId.MergeFrom(other.ItemId);
      }
      if (other.userId_ != null) {
        if (userId_ == null) {
          UserId = new global::Common.UserId();
        }
        UserId.MergeFrom(other.UserId);
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            if (itemId_ == null) {
              ItemId = new global::Common.ItemId();
            }
            input.ReadMessage(ItemId);
            break;
          }
          case 18: {
            if (userId_ == null) {
              UserId = new global::Common.UserId();
            }
            input.ReadMessage(UserId);
            break;
          }
        }
      }
    }

  }

  public sealed partial class InteractionServiceCall : pb::IMessage<InteractionServiceCall> {
    private static readonly pb::MessageParser<InteractionServiceCall> _parser = new pb::MessageParser<InteractionServiceCall>(() => new InteractionServiceCall());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<InteractionServiceCall> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Interaction.InteractionsReflection.Descriptor.MessageTypes[2]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public InteractionServiceCall() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public InteractionServiceCall(InteractionServiceCall other) : this() {
      metaData_ = other.metaData_ != null ? other.metaData_.Clone() : null;
      switch (other.MethodCase) {
        case MethodOneofCase.ConsumeItem:
          ConsumeItem = other.ConsumeItem.Clone();
          break;
      }

      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public InteractionServiceCall Clone() {
      return new InteractionServiceCall(this);
    }

    /// <summary>Field number for the "metaData" field.</summary>
    public const int MetaDataFieldNumber = 1;
    private global::Common.MetaData metaData_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.MetaData MetaData {
      get { return metaData_; }
      set {
        metaData_ = value;
      }
    }

    /// <summary>Field number for the "consumeItem" field.</summary>
    public const int ConsumeItemFieldNumber = 2;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Interaction.MonConsumeItemRequest ConsumeItem {
      get { return methodCase_ == MethodOneofCase.ConsumeItem ? (global::Interaction.MonConsumeItemRequest) method_ : null; }
      set {
        method_ = value;
        methodCase_ = value == null ? MethodOneofCase.None : MethodOneofCase.ConsumeItem;
      }
    }

    private object method_;
    /// <summary>Enum of possible cases for the "method" oneof.</summary>
    public enum MethodOneofCase {
      None = 0,
      ConsumeItem = 2,
    }
    private MethodOneofCase methodCase_ = MethodOneofCase.None;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public MethodOneofCase MethodCase {
      get { return methodCase_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void ClearMethod() {
      methodCase_ = MethodOneofCase.None;
      method_ = null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as InteractionServiceCall);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(InteractionServiceCall other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(MetaData, other.MetaData)) return false;
      if (!object.Equals(ConsumeItem, other.ConsumeItem)) return false;
      if (MethodCase != other.MethodCase) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (metaData_ != null) hash ^= MetaData.GetHashCode();
      if (methodCase_ == MethodOneofCase.ConsumeItem) hash ^= ConsumeItem.GetHashCode();
      hash ^= (int) methodCase_;
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (metaData_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(MetaData);
      }
      if (methodCase_ == MethodOneofCase.ConsumeItem) {
        output.WriteRawTag(18);
        output.WriteMessage(ConsumeItem);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (metaData_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(MetaData);
      }
      if (methodCase_ == MethodOneofCase.ConsumeItem) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(ConsumeItem);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(InteractionServiceCall other) {
      if (other == null) {
        return;
      }
      if (other.metaData_ != null) {
        if (metaData_ == null) {
          MetaData = new global::Common.MetaData();
        }
        MetaData.MergeFrom(other.MetaData);
      }
      switch (other.MethodCase) {
        case MethodOneofCase.ConsumeItem:
          if (ConsumeItem == null) {
            ConsumeItem = new global::Interaction.MonConsumeItemRequest();
          }
          ConsumeItem.MergeFrom(other.ConsumeItem);
          break;
      }

      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            if (metaData_ == null) {
              MetaData = new global::Common.MetaData();
            }
            input.ReadMessage(MetaData);
            break;
          }
          case 18: {
            global::Interaction.MonConsumeItemRequest subBuilder = new global::Interaction.MonConsumeItemRequest();
            if (methodCase_ == MethodOneofCase.ConsumeItem) {
              subBuilder.MergeFrom(ConsumeItem);
            }
            input.ReadMessage(subBuilder);
            ConsumeItem = subBuilder;
            break;
          }
        }
      }
    }

  }

  public sealed partial class FeedMonResponse : pb::IMessage<FeedMonResponse> {
    private static readonly pb::MessageParser<FeedMonResponse> _parser = new pb::MessageParser<FeedMonResponse>(() => new FeedMonResponse());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<FeedMonResponse> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Interaction.InteractionsReflection.Descriptor.MessageTypes[3]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public FeedMonResponse() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public FeedMonResponse(FeedMonResponse other) : this() {
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public FeedMonResponse Clone() {
      return new FeedMonResponse(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as FeedMonResponse);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(FeedMonResponse other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(FeedMonResponse other) {
      if (other == null) {
        return;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
        }
      }
    }

  }

  public sealed partial class InteractionServiceResponse : pb::IMessage<InteractionServiceResponse> {
    private static readonly pb::MessageParser<InteractionServiceResponse> _parser = new pb::MessageParser<InteractionServiceResponse>(() => new InteractionServiceResponse());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<InteractionServiceResponse> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Interaction.InteractionsReflection.Descriptor.MessageTypes[4]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public InteractionServiceResponse() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public InteractionServiceResponse(InteractionServiceResponse other) : this() {
      metaData_ = other.metaData_ != null ? other.metaData_.Clone() : null;
      switch (other.DataCase) {
        case DataOneofCase.Status:
          Status = other.Status.Clone();
          break;
      }

      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public InteractionServiceResponse Clone() {
      return new InteractionServiceResponse(this);
    }

    /// <summary>Field number for the "metaData" field.</summary>
    public const int MetaDataFieldNumber = 1;
    private global::Common.MetaData metaData_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.MetaData MetaData {
      get { return metaData_; }
      set {
        metaData_ = value;
      }
    }

    /// <summary>Field number for the "status" field.</summary>
    public const int StatusFieldNumber = 2;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Common.ResponseStatus Status {
      get { return dataCase_ == DataOneofCase.Status ? (global::Common.ResponseStatus) data_ : null; }
      set {
        data_ = value;
        dataCase_ = value == null ? DataOneofCase.None : DataOneofCase.Status;
      }
    }

    private object data_;
    /// <summary>Enum of possible cases for the "data" oneof.</summary>
    public enum DataOneofCase {
      None = 0,
      Status = 2,
    }
    private DataOneofCase dataCase_ = DataOneofCase.None;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public DataOneofCase DataCase {
      get { return dataCase_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void ClearData() {
      dataCase_ = DataOneofCase.None;
      data_ = null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as InteractionServiceResponse);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(InteractionServiceResponse other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(MetaData, other.MetaData)) return false;
      if (!object.Equals(Status, other.Status)) return false;
      if (DataCase != other.DataCase) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (metaData_ != null) hash ^= MetaData.GetHashCode();
      if (dataCase_ == DataOneofCase.Status) hash ^= Status.GetHashCode();
      hash ^= (int) dataCase_;
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (metaData_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(MetaData);
      }
      if (dataCase_ == DataOneofCase.Status) {
        output.WriteRawTag(18);
        output.WriteMessage(Status);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (metaData_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(MetaData);
      }
      if (dataCase_ == DataOneofCase.Status) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(Status);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(InteractionServiceResponse other) {
      if (other == null) {
        return;
      }
      if (other.metaData_ != null) {
        if (metaData_ == null) {
          MetaData = new global::Common.MetaData();
        }
        MetaData.MergeFrom(other.MetaData);
      }
      switch (other.DataCase) {
        case DataOneofCase.Status:
          if (Status == null) {
            Status = new global::Common.ResponseStatus();
          }
          Status.MergeFrom(other.Status);
          break;
      }

      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            if (metaData_ == null) {
              MetaData = new global::Common.MetaData();
            }
            input.ReadMessage(MetaData);
            break;
          }
          case 18: {
            global::Common.ResponseStatus subBuilder = new global::Common.ResponseStatus();
            if (dataCase_ == DataOneofCase.Status) {
              subBuilder.MergeFrom(Status);
            }
            input.ReadMessage(subBuilder);
            Status = subBuilder;
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
