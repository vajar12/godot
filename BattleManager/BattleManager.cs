using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using Battle.Storage;
using Common;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Grpc.Core;
using Grpc.Core.Logging;
using Monster;

namespace Battle
{
    enum EventType
    {
        BattleEvent
    }

    internal static class MonsterExtension
    {
        public static bool IsAlive(this Monster.Monster m)
        {
            if (m.BattleValues.RemainingHp > 0)
            {
                return true;
            }

            return false;
        }
    }


    public class BattleManager : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IBattleStorage _battleDb;
        private readonly IMonsterStorage _monsterDb;
        private readonly MonsterServices.MonsterServicesClient _monsterClient;
        private readonly IEventChan _IEventChan;


        public BattleManager(ILogger logger, IMonsterStorage monsterDb, IBattleStorage battleDb,
            IEventChan IEventChan)
        {
            _logger = logger;
            _battleDb = battleDb;
            _monsterDb = monsterDb;
            _IEventChan = IEventChan;


        }

        public async void ExecuteAttackCommand(Attack attack, BattleId id)
        {
            //TODO check if this throws
            var battle = _battleDb.GetBattleById(id);
            var source = _monsterDb.GetMonById(attack.Source);
            var target = _monsterDb.GetMonById(attack.Target);

            if (battle.ActiveBattle.TurnQueue[0].Id.Equals(source.Id) == false)
            {
                _logger.Error("the chosen mon is not the next one; {0} is next",
                    battle.ActiveBattle.TurnQueue[0].Id);
                throw new InvalidDataException();
            }

            var effect = CalcAttackEffect(source.BattleValues, target.BattleValues);

            _logger.Info("{0} damage was caused", effect.RemainingHp);

            target.BattleValues = AddValues(target.BattleValues, effect);

            target.Status.IsAlive = target.IsAlive();

            var res = await _monsterClient.UpdateMonAsync(target);

            NextTurn(battle);
        }
        public override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(2000, stoppingToken);
                BattleTick();
            }
        }

        //todo check for timeout and if it's ais turn 
        private async void BattleTick()
        {
            var activeBattles = _battleDb.GetActiveBattles();

            foreach (var aBattle in activeBattles)
            {
                var battle = _battleDb.GetBattleById(aBattle.Key);
                if (aBattle.Value.TurnQueue.Count == 0)
                {
                    StartNewTurn(battle);
                    return;
                }


                var nextMon = _monsterDb.GetMonById(new MonId {Id = battle.ActiveBattle.TurnQueue[0].Id});
                if (nextMon.PlayerID == -1)
                {
                    ExecuteAiTurn(battle);
                }
            }
        }


        // find suitable target and attack
        private async void ExecuteAiTurn(Battle b)
        {
            // ai turn
            var nextMon = _monsterDb.GetMonById(new MonId {Id = b.ActiveBattle.TurnQueue[0].Id});
            RepeatedField<MonId> possibleTargets;
            if (b.TeamA.Id.Id == (nextMon.PlayerID))
            {
                possibleTargets = b.TeamB.Mons;
            }
            else
            {
                possibleTargets = b.TeamA.Mons;
            }

            foreach (var m in possibleTargets)
            {
                if (_monsterDb.GetMonById(m).IsAlive())
                {
                    var attack = new Attack
                    {
                        Source = new MonId {Id = nextMon.Id},
                        Target = m
                    };
                    ExecuteAttackCommand(attack, new BattleId {Id = b.Id});
                    return;
                }
            }
        }

        public async Task<Battle> StartBattle(BattleId id)
        {
            _battleDb.StartBattle(id);


            var b = _battleDb.GetBattleById(id);
            
            

            StartNewTurn(b);

            return b;
        }

        private void NextTurn(Battle b)
        {
            b.ActiveBattle.TurnQueue.RemoveAt(0);
            
            

            if (b.ActiveBattle.TurnQueue.Count > 0)
            {
                var cleanedQ=b.ActiveBattle.TurnQueue.Where(i => _monsterDb.GetMonById(i).Status.IsAlive);
                b.ActiveBattle.TurnQueue.Clear();
                b.ActiveBattle.TurnQueue.AddRange(cleanedQ);
                

                var nextMon = _monsterDb.GetMonById(b.ActiveBattle.TurnQueue[0]);
                var turnQEvent = new TurnQueueEvent();
                turnQEvent.Mon.AddRange(b.ActiveBattle.TurnQueue);

                var battleEvent = new BattleEvent
                {
                    Id = new BattleId
                    {
                        Id = b.Id
                    },
                    TurnUpdate = turnQEvent
                };
                _logger.Debug("it's {} turn now", nextMon.Id);
                _logger.Debug("sending update to channel {0}", EventType.BattleEvent.ToString());
                _IEventChan.PublishBattleEvent(battleEvent);
                return;
            }


            StartNewTurn(b);
        }

        private async void StartNewTurn(Battle b)
        {
            b.ActiveBattle.TurnCount++;
            //fill turnqueue
            // todo use measure for order turnqueue
            var monList = b.TeamA.Mons.Concat(b.TeamB.Mons).Where(i =>  _monsterDb.GetMonById(i).Status.IsAlive);
            b.ActiveBattle.TurnQueue.AddRange(monList);
            
            
            if (b.ActiveBattle.TurnQueue.Count > 0)
            {
                var nextMon = _monsterDb.GetMonById(b.ActiveBattle.TurnQueue[0]);
                var turnQEvent = new TurnQueueEvent();
                turnQEvent.Mon.AddRange(b.ActiveBattle.TurnQueue);

                var battleEvent = new BattleEvent
                {
                    Id = new BattleId
                    {
                        Id = b.Id
                    },
                    TurnUpdate = turnQEvent
                };
                _logger.Debug("a new turn has started");
                _logger.Debug("it's {} turn now", nextMon.Id);
                _logger.Debug("sending update to channel {0}", EventType.BattleEvent.ToString());
                _IEventChan.PublishBattleEvent(battleEvent);
            }
            else
            {
                throw new Exception("weird case where no player has a living mon anymore");
            }
        }

        private static BattleValues AddValues(BattleValues a, BattleValues b)
        {
            return new BattleValues
            {
                Attack = a.Attack + b.Attack,
                Defense = a.Defense + b.Defense,
                MaxHp = a.MaxHp + b.MaxHp,
                RemainingHp = a.RemainingHp + b.RemainingHp
            };
        }

        private static BattleValues CalcAttackEffect(BattleValues source, BattleValues target)
        {
            var effect = new BattleValues {RemainingHp = -source.Attack};

            return effect;
        }
    }

    public abstract class BackgroundService
    {
        public abstract Task ExecuteAsync(CancellationToken stoppingToken);



    }
}