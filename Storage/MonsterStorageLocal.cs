using System.Collections.Concurrent;
using System.Collections.Generic;
using Common;
using Grpc.Core.Logging;
using Monster;

namespace Battle.Storage
{
    
    public interface IMonsterStorage
    {
        bool  AddMon(Monster.Monster mon);
        bool RemoveMon(MonId  id);
        Monster.Monster GetMonById(MonId  id);
        IEnumerable<Monster.Monster> GetMonstersByUserId(UserId id);

        bool UpdateMonster(Monster.Monster mon);

        //todo get pokomons in local storage, otherwise latency way too high
        // this components owns them for the time of the battle


    }
    
    public class MonsterStorageLocal:IMonsterStorage
    {
        private readonly ConcurrentDictionary<int, Monster.Monster> mons;
        private readonly ConcurrentDictionary<int, BodyValues> bodyValues;
        private readonly ConcurrentDictionary<int, BattleValues> battleValues;
        private readonly ILogger _logger;


        public MonsterStorageLocal(ILogger logger)
        {
            _logger = logger;
            this.mons=new ConcurrentDictionary<int, Monster.Monster>();
            this.bodyValues=new ConcurrentDictionary<int, BodyValues>();
            this.battleValues=new ConcurrentDictionary<int, BattleValues>();
        }
        
        

        public bool RemoveMon(MonId id)
        {

            var monId = id.Id;
           var res=mons.TryRemove(monId,out _);
           bodyValues.TryRemove(monId, out _);
           battleValues.TryRemove(monId, out _);
           
           return res;
        }

        public Monster.Monster GetMonById(MonId id)
        {
            return mons[id.Id];
        }


        public bool AddMon(Monster.Monster mon)
        {
            var bodyVal = mon.BodyValues;
            var battleVal = mon.BattleValues;

            if (bodyVal == null || battleVal == null)
            {
                _logger.Info("bodyVal or battleVal are not set for"+ mon);
                return false;
            }
            
            mons[mon.Id]=mon;
            bodyValues[mon.Id] = bodyVal;
            battleValues[mon.Id] = battleVal;
            return true;
        }
        

        public IEnumerable<Monster.Monster> GetMonstersByUserId(UserId id)
        {
            throw new System.NotImplementedException();
        }

        public bool UpdateMonster(Monster.Monster mon)
        {
            var bodyVal = mon.BodyValues;
            var battleVal = mon.BattleValues;

            if (bodyVal == null || battleVal == null)
            {
                _logger.Info("bodyVal or battleVal are not set for"+ mon);
                return false;
            }
            
            mons[mon.Id]=mon;
            bodyValues[mon.Id] = bodyVal;
            battleValues[mon.Id] = battleVal;
            return true;
        }
    }
}