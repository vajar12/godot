using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core.Logging;

namespace Battle.Storage
{
    public class BattleStorageLocal : IBattleStorage
    {
        private readonly ILogger _logger;

        private readonly ConcurrentDictionary<BattleId , Battle> _battles;
        private readonly ConcurrentDictionary<BattleId , ActiveBattle> _activeBattles;
        private readonly ConcurrentDictionary<BattleId, Queue<BattleCommand>> _commandQueue;
        private int _idCounter = 1;

        public BattleStorageLocal(ILogger logger)
        {
            _logger = logger;

            _activeBattles = new ConcurrentDictionary<BattleId , ActiveBattle>();
            _battles = new ConcurrentDictionary<BattleId , Battle>();

            BattleStorageTesting.BootstrapStore(this);
        }

        public (BattleId , StorageError) AddBattle(Battle battle)
        {
            var id = new BattleId  {Id = _idCounter};
            battle.LastUpdate=Timestamp.FromDateTime(DateTime.UtcNow);
            battle.Id = _idCounter;

            if (_battles.TryAdd(id, battle) == false) return (null, StorageError.InternalError);

            _idCounter++;
            return (id, StorageError.Ok);
        }

        public StorageError RemoveBattle(BattleId  id)
        {
            if (_battles.TryRemove(id, out var _) == false) return StorageError.NotFound;

            return StorageError.Ok;
        }

        public Battle GetBattleById(BattleId  id)
        {
            return _battles[id];
        }

        public IEnumerable<Battle> GetQuestsByUserId(UserId id)
        {
            var battleList = _battles.Select(k => k.Value).Where((x => x.TeamA.Id.Equals(id) || x.TeamB.Id.Equals(id)));

            return battleList;
        }


        //todo make threadsafe
        public StorageError StartBattle(BattleId id)
        {
            if (_battles.TryGetValue(id, out var result) == false) return StorageError.NotFound;

            
            var activeBattle=new ActiveBattle
            {
                StartTime = Timestamp.FromDateTime(DateTime.UtcNow),
                
            };

            result.ActiveBattle = activeBattle;

            if (_activeBattles.TryAdd(id, activeBattle) == false) return StorageError.InternalError;
            
            return StorageError.Ok;
        }

        public ConcurrentDictionary<BattleId, Battle> _GetBattles()
        {
            return _battles;
        }

        public ConcurrentDictionary<BattleId, ActiveBattle> GetActiveBattles()
        {
            return _activeBattles;
        }
    }

    class BattleCommand
    {    
        //Command command
        private TaskCompletionSource<ResponseStatus> blub;
        
    }
}