using System.Collections.Concurrent;
using System.Collections.Generic;
using Common;

namespace Battle.Storage
{
    public interface IBattleStorage
    {
        (BattleId , StorageError)  AddBattle(Battle battle);
        StorageError RemoveBattle(BattleId  id);
        Battle GetBattleById(BattleId  id);
        IEnumerable<Battle> GetQuestsByUserId(UserId id);
        
        //todo get pokomons in local storage, otherwise latency way too high
        // this components owns them for the time of the battle
        StorageError StartBattle(BattleId id);
        
        ConcurrentDictionary<BattleId , Battle> _GetBattles();
        ConcurrentDictionary<BattleId , ActiveBattle> GetActiveBattles();
        
      
    }

    static class BattleStorageTesting
    {
        public static void BootstrapStore(IBattleStorage storage)
        {

            var ta = new Team
            {
                Id =new UserId{Id = 1},
                Mons = { }
            };
            var tb = new Team
            {
                Id =new UserId{Id = -1},
                Mons =
                {
                    new MonId{Id = 6},
                    new MonId{Id = 7},
                }
            };

            Battle b = new Battle
            {
                TeamA =ta, TeamB = tb,
                
            };

            
            storage.AddBattle(b);
        }
    }
    public enum StorageError
    {
        NotFound,
        InternalError,
        Ok
    }
}